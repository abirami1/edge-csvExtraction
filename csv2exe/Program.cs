﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using LumenWorks.Framework.IO.Csv;
using MySql.Data.MySqlClient;
using System.Net.Http;

namespace csv2mysql
{
    class Program
    {
        static string c_date = DateTime.Now.ToString("yyyy-MM-dd");
        private static readonly HttpClient client = new HttpClient();
        public static string oDate;
        public static string MachName;
        public static string ProgName;
        public static string oProcstarttime;
        public static string oProcendtime;
        public static TimeSpan ProcTime;
        public static string oMachOprTime;
        public static string oLaserProcTime;
        public static string oNCTProcTime;
        public static TimeSpan NCTProcTime;
        public static string oAlarmTime;
        public static TimeSpan AlarmTime;
        public static string MatName;
        public static string oThickness;
        public static string oSheetX;
        public static string oSheetY;
        public static string oYieldRate;
        public static string oGood;
        public static string oNGQty;
        public static string Speed;
        static void Main(string[] args)
        {
            string connstring = "Server = localhost; Database = factory; Uid = root; Pwd = ;SslMode=none";
            MySqlConnection mcon = new MySqlConnection(connstring);
            mcon.Open();
            string connstring_setupdb = "Server = localhost; Database = setupsheetdb; Uid = root; Pwd = ;SslMode=none";
            MySqlConnection mcon_setup = new MySqlConnection(connstring_setupdb);
            mcon_setup.Open();
            try
            {
                string cur_date = DateTime.Now.ToString("yyyy-MM");
                string CmdText1 = "DELETE FROM csvquickdata WHERE idcsv!='0' ";
                using (MySqlCommand cmd1 = new MySqlCommand(CmdText1, mcon))
                {

                    cmd1.ExecuteNonQuery();
                }
                CmdText1 = "DELETE FROM csvextract WHERE date like '"+ cur_date + "%' ";
                using (MySqlCommand cmd1 = new MySqlCommand(CmdText1, mcon))
                {

                    cmd1.ExecuteNonQuery();
                }

                var files = Directory.GetFiles(@"F:\xampp\htdocs\alfadockpro\factory_layout\654\RPA", "*.csv");

                foreach (String f in files)
                {
                    string fname = System.IO.Path.GetFileNameWithoutExtension(f);
                    if (f.Contains("Ensys-ProcessingData"))
                    {
                        extract_ensis_processdata(f, mcon, mcon_setup);
                    }
                    else if (f.Contains("Bending-ProcessingData"))
                    {
                        extract_bend_processdata(f, mcon);
                    }
                    else if (f.Contains("EML-ProcessingData"))
                    {
                        extract_eml_processdata(f, mcon, mcon_setup);
                    }
                    else if (f.Contains("quick_view_data"))
                    {
                        extract_quickview_info(f, mcon, mcon_setup);
                    }
                    else if (f.Contains("OEEData"))
                    {
                        extract_oeee_info(f, mcon);
                    }
                    else if (f.Contains("DailyProductInfo"))
                    {
                        extract_product_info(f, mcon);
                    }
                    else if (f.Contains("CumulativeProduction"))
                    {
                        extract_cum_product_info(f, mcon);
                    }
                }
            }
            catch (Exception e)
            {
                using (StreamWriter w = File.AppendText("log.txt"))
                {
                    Log("Main" + e, w);
                }
            }
            finally
            {
                if (mcon != null)
                {
                    mcon.Close();
                }
                if (mcon_setup != null)
                {
                    mcon_setup.Close();
                }
            }
        }
        public static void extract_quickview_info(string path, MySqlConnection mcon, MySqlConnection mcon2)
        {
            string val_id = "";
            if (path.Contains("Ensys"))
            {
                val_id = "en";

            }else if (path.Contains("EN6225AJ"))
            {
                val_id = "en6";

            }
            else if (path.Contains("HDS-13"))
            {
                val_id = "13hds";
            }
            else if (path.Contains("HDS-17"))
            {
                val_id = "17hds";
            }
            else if (path.Contains("HDS-22"))
            {
                val_id = "22hds";
            }
            else if (path.Contains("EML"))
            {
                val_id = "em";
            }
            else if (path.Contains("FMB"))
            {
                val_id = "fm";
            }
            
       string fileName;
        string Act_fileName;
         string MachName;
         string ProgNo;
        string procstrt;
         string procend;
         string goodqty;
         string defqty;
         TimeSpan ProcTime;
            try
            {
                using (CsvReader csv = new CsvReader(new StreamReader(path), true))
                {
                    int counter = 1;
                    fileName = Path.GetFileNameWithoutExtension(path);
                    string[] filen = fileName.Split('-');
                    Act_fileName = filen[0].Trim();
                    if (Act_fileName == "Ensys")
                    {
                        Act_fileName = "ENSIS40";
                    }else if (Act_fileName.StartsWith("EN6225AJ"))
                    {
                        Act_fileName = "ENSIS62";
                    }
                    else if (fileName.Contains("FMB") || fileName.Contains("HDS-17") || fileName.Contains("HDS-13") || fileName.Contains("HDS-22"))
                    {
                        Act_fileName = (fileName.Split('-')[0] + "-" + fileName.Split('-')[1]).Trim();
                    }


                    int fieldCount = csv.FieldCount;
                    string[] headers = csv.GetFieldHeaders();
                    foreach (string headval in headers)
                    {
                        if (headval.Contains("アラームメッセージ"))
                            return;
                    }

                    while (csv.ReadNextRecord())
                    {


                        //Machine Name
                        MachName = Act_fileName;

                        
                        //Program Name
                        ProgNo = csv[1];

                        DateTime Date1 = new DateTime();
                        DateTime Date2 = new DateTime();
                        if (Act_fileName.Contains("ENSIS") || Act_fileName.Contains("EML"))
                        {
                            //Process start time
                            procstrt = csv[4];
                            Date1 = DateTime.Parse(procstrt);

                            //Process end time
                            procend = csv[5];
                            Date2 = DateTime.Parse(procend);

                            //Good Qty
                            goodqty = csv[7];

                            //Defective Qty
                            defqty = csv[8];
                            //Process Time
                            ProcTime = Date2 - Date1;

                            int def_qty = Convert.ToInt16(defqty);
                            string str_machname = "EML3610NT";
                            if (Act_fileName.Contains("ENSIS40"))
                            {
                                str_machname = "ENSIS4020AJ";
                            }
                            if (Act_fileName.Contains("ENSIS62"))
                            {
                                str_machname = "ENSIS6225AJ";
                            }
                            if (def_qty != 1 && !String.IsNullOrEmpty(ProgNo))
                            {

                                updateEDGEpgminfo(ProgNo, procstrt, procend, str_machname, mcon2);
                                if (str_machname.Equals("ENSIS4020AJ") || str_machname.Equals("ENSIS6225AJ"))
                                {
                                    ProgNo = ProgNo.Replace(".0", "");
                                    ProgNo = ProgNo.Replace(".0W", "");

                                    if (ProgNo.Contains("-") && ProgNo.Contains("+"))
                                    {
                                        int dash_count = ProgNo.Length - ProgNo.Replace("-", "").Length;
                                        if (dash_count == 2)
                                        {//267121-2+143-3+4
                                            String[] prog_split = ProgNo.Split('-');
                                            string main_prog = prog_split[0];
                                            int m_val = 0;
                                            foreach (string main_app_val in prog_split)
                                            {
                                                if (m_val == 0) {
                                                    main_prog = prog_split[0];
                                                }
                                                else
                                                {
                                                    String[] append_split = prog_split[m_val].Split('+');
                                                    int c_val = 1;
                                                    foreach (string app_val in append_split)
                                                    {
                                                        if (c_val == 1 || app_val.Length<3)
                                                            updateEDGEpgminfo(main_prog + "-" + app_val, procstrt, procend, str_machname, mcon2);
                                                        else
                                                        {
                                                            int len_newval = app_val.Length;
                                                            string new_pg = main_prog.Substring(0, (main_prog.Length - len_newval)) + app_val;
                                                            main_prog = new_pg;
                                                        }
                                                        c_val++;
                                                    }
                                                }
                                                m_val++;
                                            }
                                            }
                                        else
                                        {
                                            String[] prog_split = ProgNo.Split('-');
                                            string main_prog = prog_split[0];
                                            
                                            //2666686+637-1
                                            if (main_prog.Contains("+"))
                                            {
                                                String[] prog1_split = main_prog.Split('+');
                                                string main_prog_1 = prog1_split[0];
                                                updateEDGEpgminfo(main_prog_1 + "-" + prog_split[1], procstrt, procend, str_machname, mcon2);
                                                int counter_val = 1;

                                                foreach (string app_val in prog1_split)
                                                {
                                                    if (counter_val != 1)
                                                    {
                                                        int len_newval = app_val.Length;
                                                        string new_pg = main_prog_1.Substring(0, (main_prog_1.Length - len_newval)) + app_val;
                                                        updateEDGEpgminfo(new_pg + "-" + prog_split[1], procstrt, procend, str_machname, mcon2);

                                                    }
                                                    counter_val++;
                                                }
                                            }
                                            else
                                            {
                                                String tot_pgval = "";
                                                String[] append_split = prog_split[1].Split('+');
                                                foreach (string app_val in append_split)
                                                {   if (!String.IsNullOrEmpty(tot_pgval))
                                                        tot_pgval = tot_pgval + "#" + main_prog + "-" + app_val;
                                                    else
                                                        tot_pgval =  main_prog + "-" + app_val;
                                                    updateEDGEpgminfo(main_prog + "-" + app_val, procstrt, procend, str_machname, mcon2);

                                                }
                                                updateEDGEpgminfo(tot_pgval, procstrt, procend, str_machname, mcon2);

                                            }
                                        }
                                    }
                                    else if (!ProgNo.Contains("-") && ProgNo.Contains("+"))
                                      {
                                          String[] prog_split = ProgNo.Split('+');
                                          string main_prog = prog_split[0];
                                        updateEDGEpgminfo_match(main_prog, procstrt, procend, str_machname, mcon2);

                                          int counter_val = 1;

                                          foreach (string app_val in prog_split)
                                          {
                                              if (counter_val != 1)
                                              {
                                                  int len_newval = app_val.Length;
                                                  string new_pg = main_prog.Substring(0, (main_prog.Length - len_newval)) + app_val;
                                                updateEDGEpgminfo_match(new_pg, procstrt, procend, str_machname, mcon2);

                                              }
                                              counter_val++;
                                          }

                                      }
                                    else if (ProgNo.Contains("-") && ProgNo.EndsWith("P"))
                                    {
                                        String[] prog_split = ProgNo.Split('-');
                                        string main_prog = prog_split[0];
                                        int count = 1;
                                        foreach (string app_val in prog_split)
                                        {
                                            if (count != 1)
                                            {
                                                string app_val1 = app_val.Replace("P", "");
                                                updateEDGEpgminfo(main_prog + "-" + app_val1, procstrt, procend, str_machname, mcon2);

                                            }
                                            count++;
                                        }

                                    }
                                    else if (!ProgNo.Contains("-") && !ProgNo.Contains("+"))
                                    {
                                        updateEDGEpgminfo_match(ProgNo, procstrt, procend, str_machname, mcon2);
                                  
                                    }
                                    else
                                        updateEDGEpgminfo(ProgNo, procstrt, procend, str_machname, mcon2);

                                }
                                
                            }
                        }
                        else
                        {
                            //Process start time
                            procstrt = csv[3];
                            Date1 = DateTime.Parse(procstrt);

                            //Process end time
                            procend = csv[4];
                            Date2 = DateTime.Parse(procend);

                            //Good Qty
                            goodqty = csv[5];

                            //Defective Qty
                            defqty = csv[6];
                            //Process Time
                            ProcTime = Date2 - Date1;
                        }
                        double ProcTimeSecs = ProcTime.TotalSeconds;


                        string CmdText = "INSERT INTO csvquickdata VALUES(@idcsv,@machname,@progno,@procstrt,@procend,@goodqty,@defqty,@proctimesecs)";
                        MySqlCommand cmd = new MySqlCommand(CmdText, mcon);
                        cmd.Parameters.AddWithValue("@idcsv", val_id + "-" + counter);
                        cmd.Parameters.AddWithValue("@machname", MachName);
                        cmd.Parameters.AddWithValue("@progno", ProgNo);
                        cmd.Parameters.AddWithValue("@procstrt", Date1);
                        cmd.Parameters.AddWithValue("@procend", Date2);
                        cmd.Parameters.AddWithValue("@goodqty", goodqty);
                        cmd.Parameters.AddWithValue("@defqty", defqty);
                        cmd.Parameters.AddWithValue("@proctimesecs", ProcTimeSecs);

                        cmd.ExecuteNonQuery();
                        counter++;

                    }
                }
            }catch(Exception e)
            {
                using (StreamWriter w = File.AppendText("log.txt"))
                {
                    Log("Quick" + e, w);
                }
            }
                }
        async void update_GPN(String partno, string pid, string qt, string userid, bool str_datefilter, string orderno, string start_date, string end_date, string due_date)
        {
            var str_arg = "{\"partNo\":\"" + partno + "\",\"processid\":\"" + pid + "\",\"status\":\"2\",\"quantity\":\"" + qt + "\",\"userid\":\"" + userid + "\",\"isSearchApplyDateFilters\":\"false\",\"setGivenTime\":\"true\",\"orderNo\":\"" + orderno + "\",\"startdate\":\"" + start_date + "\",\"enddate\":\"" + end_date + "\",\"dueDate\":\"" + due_date + "\"}";
            using (StreamWriter w = File.AppendText("log.txt"))
            {
                Log(str_arg, w);
            }
            var values = new Dictionary<string, string>
{
    { "plugin", "SchedulerAPI" },
    { "controller", "SchedulerSubmitProcessController" },
    { "action", "SiProcessForLoadBalance" },
    { "args", str_arg }
};

            var content = new FormUrlEncodedContent(values);

            var response = await client.PostAsync("https://www.alfadock-pack.com/api/plugin", content);

            //var responseString = 
            await response.Content.ReadAsStringAsync();

        }
        public static void updateEDGEpgminfo(string partno, string sdate, string edate,string machname, MySqlConnection mcon)
        {
            int pid = 17549;
            if (machname.Equals("ENSIS4020AJ")){
                pid = 17549;
            }
            
            Program pg = new Program();

            try
            {

               

                string query = string.Format(@"UPDATE `setupsheetdb`.setupsheetedge SET processed=2,starttime='{1}',endtime='{2}'  where machinename='{3}' AND (programname='{0}' OR (partnum='{0}')) AND (starttime='0000-00-00 00:00:00' OR endtime='0000-00-00 00:00:00') AND processed!=2", partno, sdate,edate, machname);

                using (MySqlCommand command = new MySqlCommand(query, mcon))
                {
                    int row = command.ExecuteNonQuery();
                    if(row == 1)
                    {
                        string sel_query = string.Format(@"SELECT partnum,comqty FROM `setupsheetdb`.setupsheetedgebend where  (programname='{0}' OR (partnum='{0}'))", partno);

                        using (MySqlCommand command1 = new MySqlCommand(sel_query, mcon))
                        {
                            MySqlDataReader reader = command1.ExecuteReader();
                            if (reader.HasRows)
                            {
                                while (reader.Read())
                                {


                                    string part_no = reader["partnum"].ToString();
                                    string qty = reader["comqty"].ToString();
                                    if (!part_no.StartsWith("00"))
                                        part_no = "00" + part_no;
                                    pg.update_GPN(part_no, "" + pid, qty, "2741", false, "-1", sdate.Replace("/","-"), edate.Replace("/", "-"), c_date);
                                    

                                }
                            }

                        }
                    }
                }


            }
            catch (Exception)
            {
            }
            finally
            {
                
            }


        }
        public static void Log(string logMessage, TextWriter w)
        {
            w.Write("\r\nLog Entry : ");
            w.WriteLine($"{DateTime.Now.ToLongTimeString()} {DateTime.Now.ToLongDateString()}");
            w.WriteLine("  :");
            w.WriteLine($"  :{logMessage}");
            w.WriteLine("-------------------------------");
        }
        public static void updateEDGEpgminfo_match(string partno, string sdate, string edate, string machname, MySqlConnection mcon)
        {
            int pid = 17549;
            if (machname.Equals("ENSIS4020AJ") || machname.Equals("ENSIS6225AJ"))
            {
                pid = 17549;
            }

            Program pg = new Program();


            try
            {

                string query = string.Format(@"UPDATE `setupsheetdb`.setupsheetedge SET processed=2,starttime='{1}',endtime='{2}'  where machinename='{3}' AND (programname like '{0}%' OR (partnum like '{0}%' AND (schedulename='' OR schedulename='schedule')) OR schedulename like '{0}%') AND (starttime='0000-00-00 00:00:00' OR endtime='0000-00-00 00:00:00') AND processed!=2", partno, sdate, edate, machname);

                using (MySqlCommand command = new MySqlCommand(query, mcon))
                {
                    int row = command.ExecuteNonQuery();
                    if (row == 1)
                    {
                        string sel_query = string.Format(@"SELECT partnum,comqty FROM `setupsheetdb`.setupsheetedgebend where  (programname like '{0}%' OR (partnum like '{0}%') OR schedulename like '{0}%')", partno);

                        using (MySqlCommand command1 = new MySqlCommand(sel_query, mcon))
                        {
                            MySqlDataReader reader = command1.ExecuteReader();
                            if (reader.HasRows)
                            {
                                while (reader.Read())
                                {
                                    string part_no = reader["partnum"].ToString();
                                    string qty = reader["comqty"].ToString();
                                    if (!part_no.StartsWith("00"))
                                        part_no = "00" + part_no;
                                    pg.update_GPN(part_no, "" + pid, qty, "2741", false, "-1", sdate.Replace("/", "-"), edate.Replace("/", "-"), c_date);

                                    using (StreamWriter w = File.AppendText("log.txt"))
                                    {
                                        Log("part_no:" + part_no+ pid+qty+ sdate.Replace("/", "-") + edate.Replace("/", "-"), w);
                                    }
                                }
                            }

                        }
                    }
                }


            }
            catch (Exception)
            {
            }
            finally
            {

            }


        }
        public static void extract_oeee_info(string path, MySqlConnection mcon)
        {
            string val_id = "";
            if (path.Contains("Ensys"))
            {
                val_id = "en";

            }
            else if (path.Contains("Bending"))
            {
                val_id = "ben";
            }
            else if (path.Contains("EML"))
            {
                val_id = "em";
            }

            string MachName;
        
         string date;
            float optime;
            float settime;
            float idletime;
            float errtime;
            using (CsvReader csv =
             new CsvReader(new StreamReader(path), true))
            {
                int counter = 1;
                int fieldCount = csv.FieldCount;
                string[] headers = csv.GetFieldHeaders();
                while (csv.ReadNextRecord())
                {
                    //for (int i = 0; i < fieldCount; i++)
                    //Console.Write(string.Format("{0} = {1};",headers[i], csv[i]));


                    //Machine Name
                    MachName = csv[0];

                    //Date
                    date = csv[1];

                   
                    //time
                    optime = float.Parse(csv[2]);
                    settime = float.Parse(csv[3]);
                    idletime = float.Parse(csv[4]);
                    errtime = float.Parse(csv[5]);

                    float Totaltime = optime + settime + idletime + errtime;
                    float op_percent = optime;
                    float set_percent = settime;
                    float idle_percent = idletime;
                    float err_percent = errtime;
                  /*  if (Totaltime != 0)
                    {

                         op_percent = (optime * 100) / Totaltime;
                        set_percent = (settime * 100) / Totaltime;
                         idle_percent = (idletime * 100) / Totaltime;
                         err_percent = (errtime * 100) / Totaltime;
                    }*/

                    string CmdText = "INSERT IGNORE INTO csvtime VALUES(@idcsv,@machname,@date,@optime,@settime,@idletime,@errtime,@tottime,@avgvalue)";
                    MySqlCommand cmd = new MySqlCommand(CmdText, mcon);
                    cmd.Parameters.AddWithValue("@idcsv", date+"_"+val_id + "_"+counter);
                    cmd.Parameters.AddWithValue("@machname", Encoding.UTF8.GetBytes(MachName));
                    cmd.Parameters.AddWithValue("@date", date);
                    cmd.Parameters.AddWithValue("@optime", op_percent);
                    cmd.Parameters.AddWithValue("@settime", set_percent);
                    cmd.Parameters.AddWithValue("@idletime", idle_percent);
                    cmd.Parameters.AddWithValue("@errtime", err_percent);
                    cmd.Parameters.AddWithValue("@tottime", Totaltime);
                    cmd.Parameters.AddWithValue("@avgvalue", 100);


                    int result = cmd.ExecuteNonQuery();
                    if (result != 1)
                    {
                        string query = string.Format(@"UPDATE csvtime SET optime={1} , settime={2}, idletime={3}, errtime={4}, tottime={5} where NOT tottime ={5} AND idcsv='{0}'", date + "_" + val_id + "_" + counter, op_percent, set_percent, idle_percent, err_percent, Totaltime);


                        using (MySqlCommand command = new MySqlCommand(query, mcon))
                        {
                            int row = command.ExecuteNonQuery();
                        }
                    }
                    counter++;

                }
            }
        }

        public static void extract_cum_product_info(string path, MySqlConnection mcon)
        {
            string val_id = "";
            if (path.Contains("Ensys"))
            {
                val_id = "en";

            }
            else if (path.Contains("Bending"))
            {
                val_id = "b";
            }
            else if (path.Contains("EML"))
            {
                val_id = "em";
            }
            string MachName;
            string Str_ActProcCount;
            string Str_TargetCount;
            using (CsvReader csv = new CsvReader(new StreamReader(path), true))
            {
                int counter = 1;
                int fieldCount = csv.FieldCount;
                string[] headers = csv.GetFieldHeaders();
                while (csv.ReadNextRecord())
                {
                    //for (int i = 0; i < fieldCount; i++)
                    //Console.Write(string.Format("{0} = {1};",headers[i], csv[i]));


                    //Machine Name
                    MachName = csv[0];

                    //Actual Process Count
                    Str_ActProcCount = csv[1];

                    //Target Process Count
                    Str_TargetCount = csv[2];

                   

                    string CmdText = "INSERT IGNORE INTO csvcumprocess VALUES(@idcsv,@machname,@actproccount,@targetcount)";
                    MySqlCommand cmd = new MySqlCommand(CmdText, mcon);
                    cmd.Parameters.AddWithValue("@idcsv", "2020-05-31" + "_" + val_id + "_" + counter);
                    cmd.Parameters.AddWithValue("@machname", Encoding.UTF8.GetBytes(MachName));
                    cmd.Parameters.AddWithValue("@actproccount", Str_ActProcCount);
                    cmd.Parameters.AddWithValue("@targetcount", Str_TargetCount);

                    
                    int result = cmd.ExecuteNonQuery();
                    if (result != 1)
                    {
                        string query = string.Format(@"UPDATE csvcumprocess SET actproccount={1}, targetcount={2} where (NOT actproccount ={1}) AND idcsv='{0}'", "2020-05-31" + "_" + val_id + "_" + counter, Str_ActProcCount, Str_TargetCount);


                        using (MySqlCommand command = new MySqlCommand(query, mcon))
                        {
                            int row = command.ExecuteNonQuery();
                        }
                    }
                    counter++;
                }

            }
        }
        
        public static void extract_product_info(string path, MySqlConnection mcon)
        {
            string val_id = "";
            if (path.Contains("Ensys"))
            {
                val_id = "en";

            }
            else if (path.Contains("Bending"))
            {
                val_id = "ben";
            }
            else if (path.Contains("EML"))
            {
                val_id = "em";
            }
            string MachName;
         string str_Date;
        string Str_ActProcCount;
         string Str_TargetCount;
            using (CsvReader csv =new CsvReader(new StreamReader(path), true))
            {
                int counter = 1;
                int fieldCount = csv.FieldCount;
                string[] headers = csv.GetFieldHeaders();
                while (csv.ReadNextRecord())
                {
                    //for (int i = 0; i < fieldCount; i++)
                    //Console.Write(string.Format("{0} = {1};",headers[i], csv[i]));


                    //Machine Name
                    MachName = csv[0];

                    //Actual Process Count
                    Str_ActProcCount = csv[4];

                    //Target Process Count
                    Str_TargetCount = csv[5];

                    //Date
                    str_Date = csv[1] + "/" + csv[2] + "/" + csv[3];
                    DateTime Date = DateTime.Parse(str_Date);

                    string str_Date_1 = csv[1] + "-" + csv[2] + "-" + csv[3];

                    string CmdText = "INSERT IGNORE INTO csvprocess VALUES(@idcsv,@machname,@date,@actproccount,@targetcount)";
                    MySqlCommand cmd = new MySqlCommand(CmdText, mcon);
                    cmd.Parameters.AddWithValue("@idcsv", str_Date_1 + "_" + val_id +"_"+counter);
                    cmd.Parameters.AddWithValue("@machname", Encoding.UTF8.GetBytes(MachName));
                    cmd.Parameters.AddWithValue("@date", Date);
                    cmd.Parameters.AddWithValue("@actproccount", Str_ActProcCount);
                    cmd.Parameters.AddWithValue("@targetcount", Str_TargetCount);
                    
                   
                    int result = cmd.ExecuteNonQuery();
                    if (result != 1)
                    {
                        string query = string.Format(@"UPDATE csvprocess SET actproccount={1}, targetcount={2} where NOT actproccount ={1} AND idcsv='{0}'", str_Date_1 + "_" + val_id + "_" + counter, Str_ActProcCount, Str_TargetCount);


                        using (MySqlCommand command = new MySqlCommand(query, mcon))
                        {
                            int row = command.ExecuteNonQuery();
                        }
                    }
                    counter++;
                }

            }
        }

    public static void extract_bend_processdata(string path, MySqlConnection mcon)
        {
            
            using (CsvReader csv =
                        new CsvReader(new StreamReader(path), true))
        {

                int counter = 1;
            int fieldCount = csv.FieldCount;
            string[] headers = csv.GetFieldHeaders();
            while (csv.ReadNextRecord())
            {
                //for (int i = 0; i < fieldCount; i++)
                //Console.Write(string.Format("{0} = {1};",headers[i], csv[i]));

                // Date
                oDate = csv[0];
                DateTime Date = DateTime.Parse(oDate);

                //Machine Name
                MachName = csv[1];

                //Program Name
                ProgName = csv[2];

                //Process start time
                oProcstarttime = csv[4];
                DateTime Procstarttime = DateTime.Parse(oProcstarttime);


                //Process end time
                oProcendtime = csv[5];
                DateTime Procendtime = DateTime.Parse(oProcendtime);

                //Processing Time
                ProcTime = Procendtime - Procstarttime;
                double ProcTimeSecs = ProcTime.TotalSeconds;

                //Machine Operation Time
                oMachOprTime = csv[6];
                TimeSpan MachOprTime = TimeSpan.Parse(oMachOprTime);
                double OprTimeSecs = MachOprTime.TotalSeconds;


                //Alarm Time
                oAlarmTime = csv[7];
                if (oAlarmTime.Contains("-"))
                {
                    oAlarmTime = "00:00:00";
                }
                AlarmTime = TimeSpan.Parse(oAlarmTime);
                double AlarmTimeSecs = AlarmTime.TotalSeconds;

                //Material name
                MatName = csv[8];


                string CmdText = "INSERT IGNORE INTO csvextract VALUES(@idcsv,@date,@machname,@progname,@procstrtime,@procendtime,@proctime,@machoprtime,@laserproctime,@nctproctime,@alarmtime,@matname,@thickness,@sheetx,@sheety,@yieldrate,@good,@ngqty,@speed,@proctimesecs,@alarmtimesecs,@oprtimesecs)";
                MySqlCommand cmd = new MySqlCommand(CmdText, mcon);

                cmd.Parameters.AddWithValue("@idcsv", oDate + "_" + "be" +counter);
                cmd.Parameters.AddWithValue("@date", Date);
                    cmd.Parameters.AddWithValue("@machname", Encoding.UTF8.GetBytes(MachName));
                    cmd.Parameters.AddWithValue("@progname", ProgName);
                cmd.Parameters.AddWithValue("@procstrtime", Procstarttime);
                cmd.Parameters.AddWithValue("@procendtime", Procendtime);
                cmd.Parameters.AddWithValue("@proctime", ProcTime);
                cmd.Parameters.AddWithValue("@machoprtime", MachOprTime);
                cmd.Parameters.AddWithValue("@laserproctime", "00:00:00");
                cmd.Parameters.AddWithValue("@nctproctime", NCTProcTime);
                cmd.Parameters.AddWithValue("@alarmtime", AlarmTime);
                cmd.Parameters.AddWithValue("@matname", MatName);
                cmd.Parameters.AddWithValue("@thickness", 0);
                cmd.Parameters.AddWithValue("@sheetx", 0);
                cmd.Parameters.AddWithValue("@sheety", 0);
                cmd.Parameters.AddWithValue("@yieldrate", 0);
                cmd.Parameters.AddWithValue("@good", oGood);
                cmd.Parameters.AddWithValue("@ngqty", oNGQty);
                cmd.Parameters.AddWithValue("@speed", Speed);
                cmd.Parameters.AddWithValue("@proctimesecs", ProcTimeSecs);
                cmd.Parameters.AddWithValue("@alarmtimesecs", AlarmTimeSecs);

                cmd.Parameters.AddWithValue("@oprtimesecs", OprTimeSecs);

                cmd.ExecuteNonQuery();
                    counter++;
            }
        }
    }
        public static void extract_eml_processdata(string path, MySqlConnection mcon, MySqlConnection mcon2)
        {
            using (CsvReader csv =
                        new CsvReader(new StreamReader(path), true))
            {
                int counter = 1;
                int fieldCount = csv.FieldCount;
                string[] headers = csv.GetFieldHeaders();
                while (csv.ReadNextRecord())
                {
                    //for (int i = 0; i < fieldCount; i++)
                    //Console.Write(string.Format("{0} = {1};",headers[i], csv[i]));

                    //Date
                    oDate = csv[1];
                    DateTime Date = DateTime.Parse(oDate);

                    //Machine Name
                    MachName = csv[2];

                    //Program Name
                    ProgName = csv[3];

                    //Process start time
                    oProcstarttime = csv[8];
                    DateTime Procstarttime = DateTime.Parse(oProcstarttime);


                    //Process end time
                    oProcendtime = csv[9];
                    DateTime Procendtime = DateTime.Parse(oProcendtime);

                    //Processing Time
                    ProcTime = Procendtime - Procstarttime;
                    double ProcTimeSecs = ProcTime.TotalSeconds;

                    //Machine Operation Time
                    oMachOprTime = csv[10];
                    TimeSpan MachOprTime = TimeSpan.Parse(oMachOprTime);

                    double OprTimeSecs = MachOprTime.TotalSeconds;

                    //Laser processing time
                    oLaserProcTime = csv[11];
                    TimeSpan LaserProcTime = TimeSpan.Parse(oLaserProcTime);

                    //NCT processing time
                    oNCTProcTime = csv[12];
                    if (oNCTProcTime.Contains("-"))
                    {
                        oNCTProcTime = "00:00:00";
                    }
                    NCTProcTime = TimeSpan.Parse(oNCTProcTime);

                    //Alarm Time
                    oAlarmTime = csv[13];
                    if (oAlarmTime.Contains("-"))
                    {
                        oAlarmTime = "00:00:00";
                    }
                    AlarmTime = TimeSpan.Parse(oAlarmTime);
                    double AlarmTimeSecs = AlarmTime.TotalSeconds;

                    //Material name
                    MatName = csv[14];

                    //Thickness
                    oThickness = csv[15];
                    float Thickness = float.Parse(oThickness);

                    //SheetX
                    oSheetX = csv[16];
                    oSheetX = oSheetX.Remove(oSheetX.Length - 3);
                    int SheetX = Int32.Parse(oSheetX);

                    //SheetY
                    oSheetY = csv[17];
                    oSheetY = oSheetY.Remove(oSheetY.Length - 3);
                    int SheetY = Int32.Parse(oSheetY);

                    //Yield rate
                    oYieldRate = csv[18];
                    float YieldRate = float.Parse(oYieldRate);

                    //Good
                    oGood = csv[19];
                    //bool Good = bool.Parse(oGood);
                    //bool Good = Convert.ToBoolean(oGood);

                    //NG Qty
                    oNGQty = csv[20];
                    //bool NGQty = bool.Parse(oNGQty);

                    //Speed
                    Speed = csv[21];

                   /* int def_qty = Convert.ToInt16(oNGQty);
                    if (def_qty != 1 && !String.IsNullOrEmpty(ProgName))
                    {
                            updateEDGEpgminfo(ProgName, oProcstarttime, oProcendtime, "EML3610NT", mcon2);
                    }
                    */
                    string CmdText = "INSERT IGNORE INTO csvextract VALUES(@idcsv,@date,@machname,@progname,@procstrtime,@procendtime,@proctime,@machoprtime,@laserproctime,@nctproctime,@alarmtime,@matname,@thickness,@sheetx,@sheety,@yieldrate,@good,@ngqty,@speed,@proctimesecs,@alarmtimesecs,@oprtimesecs)";
                    MySqlCommand cmd = new MySqlCommand(CmdText, mcon);

                    cmd.Parameters.AddWithValue("@idcsv", oDate + "_" + "em" + counter);
                    cmd.Parameters.AddWithValue("@date", Date);
                    cmd.Parameters.AddWithValue("@machname", Encoding.UTF8.GetBytes(MachName));
                    cmd.Parameters.AddWithValue("@progname", ProgName);
                    cmd.Parameters.AddWithValue("@procstrtime", Procstarttime);
                    cmd.Parameters.AddWithValue("@procendtime", Procendtime);
                    cmd.Parameters.AddWithValue("@proctime", ProcTime);
                    cmd.Parameters.AddWithValue("@machoprtime", MachOprTime);
                    cmd.Parameters.AddWithValue("@laserproctime", LaserProcTime);
                    cmd.Parameters.AddWithValue("@nctproctime", NCTProcTime);
                    cmd.Parameters.AddWithValue("@alarmtime", AlarmTime);
                    cmd.Parameters.AddWithValue("@matname", MatName);
                    cmd.Parameters.AddWithValue("@thickness", Thickness);
                    cmd.Parameters.AddWithValue("@sheetx", SheetX);
                    cmd.Parameters.AddWithValue("@sheety", SheetY);
                    cmd.Parameters.AddWithValue("@yieldrate", YieldRate);
                    cmd.Parameters.AddWithValue("@good", oGood);
                    cmd.Parameters.AddWithValue("@ngqty", oNGQty);
                    cmd.Parameters.AddWithValue("@speed", Speed);
                    cmd.Parameters.AddWithValue("@proctimesecs", ProcTimeSecs);
                    cmd.Parameters.AddWithValue("@alarmtimesecs", AlarmTimeSecs);

                    cmd.Parameters.AddWithValue("@oprtimesecs", OprTimeSecs);

                    cmd.ExecuteNonQuery();
                    counter++;
                }

            }
        }
        public static void extract_ensis_processdata(string path, MySqlConnection mcon,MySqlConnection mcon2)
        {
            using (CsvReader csv =
                        new CsvReader(new StreamReader(path), true))
            {
                int counter = 1;
                int fieldCount = csv.FieldCount;
                string[] headers = csv.GetFieldHeaders();
                while (csv.ReadNextRecord())
                {
                    //for (int i = 0; i < fieldCount; i++)
                    //Console.Write(string.Format("{0} = {1};",headers[i], csv[i]));

                    //Date
                    oDate = csv[1];
                    DateTime Date = DateTime.Parse(oDate);

                    //Machine Name
                    MachName = csv[2];

                    //Program Name
                    ProgName = csv[3];

                    //Process start time
                    oProcstarttime = csv[8];
                    DateTime Procstarttime = DateTime.Parse(oProcstarttime);


                    //Process end time
                    oProcendtime = csv[9];
                    DateTime Procendtime = DateTime.Parse(oProcendtime);

                    //Processing Time
                    ProcTime = Procendtime - Procstarttime;
                    double ProcTimeSecs = ProcTime.TotalSeconds;

                    //Machine Operation Time
                    oMachOprTime = csv[10];
                    TimeSpan MachOprTime = TimeSpan.Parse(oMachOprTime);

                    double OprTimeSecs = MachOprTime.TotalSeconds;

                    //Laser processing time
                    oLaserProcTime = csv[11];
                    TimeSpan LaserProcTime = TimeSpan.Parse(oLaserProcTime);

                    //NCT processing time
                    oNCTProcTime = csv[12];
                    if (oNCTProcTime.Contains("-"))
                    {
                        oNCTProcTime = "00:00:00";
                    }
                    NCTProcTime = TimeSpan.Parse(oNCTProcTime);

                    //Alarm Time
                    oAlarmTime = csv[13];
                    if (oAlarmTime.Contains("-"))
                    {
                        oAlarmTime = "00:00:00";
                    }
                    AlarmTime = TimeSpan.Parse(oAlarmTime);
                    double AlarmTimeSecs = AlarmTime.TotalSeconds;

                    //Material name
                    MatName = csv[14];

                    //Thickness
                    oThickness = csv[15];
                    float Thickness = float.Parse(oThickness);

                    //SheetX
                    oSheetX = csv[16];
                    oSheetX = oSheetX.Remove(oSheetX.Length - 3);
                    int SheetX = Int32.Parse(oSheetX);

                    //SheetY
                    oSheetY = csv[17];
                    oSheetY = oSheetY.Remove(oSheetY.Length - 3);
                    int SheetY = Int32.Parse(oSheetY);

                    //Yield rate
                    oYieldRate = csv[18];
                    float YieldRate = float.Parse(oYieldRate);

                    //Good
                    oGood = csv[19];
                    //bool Good = bool.Parse(oGood);
                    //bool Good = Convert.ToBoolean(oGood);

                    //NG Qty
                    oNGQty = csv[20];
                    //bool NGQty = bool.Parse(oNGQty);

                    //Speed
                    Speed = csv[21];
                   /* int def_qty = Convert.ToInt16(oNGQty);
                    if (def_qty != 1 && !String.IsNullOrEmpty(ProgName))
                    {

                        if (ProgName.Contains("-") && ProgName.Contains("+"))
                                    {
                            String[] prog_split = ProgName.Split('-');
                            string main_prog = prog_split[0];
                            if (main_prog.Contains("+"))
                            {
                                String[] prog1_split = main_prog.Split('+');
                                string main_prog_1 = prog1_split[0];
                                updateEDGEpgminfo(main_prog_1 + "-" + prog_split[1], oProcstarttime, oProcendtime, "ENSIS4020AJ", mcon2);
                                int counter_val = 1;

                                foreach (string app_val in prog1_split)
                                {
                                    if (counter_val != 1)
                                    {
                                        int len_newval = app_val.Length;
                                        string new_pg = main_prog_1.Substring(0, (main_prog_1.Length - len_newval)) + app_val;
                                        updateEDGEpgminfo(new_pg + "-" + prog_split[1], oProcstarttime, oProcendtime, "ENSIS4020AJ", mcon2);

                                    }
                                    counter_val++;
                                }
                            }
                            else {
                              
                                String[] append_split = prog_split[1].Split('+');
                                string tot_pgval = "";
                                foreach (string app_val in append_split)
                                {
                                    if (!String.IsNullOrEmpty(tot_pgval))
                                        tot_pgval = tot_pgval + "#" + main_prog + "-" + app_val;
                                    else
                                        tot_pgval = main_prog + "-" + app_val;
                                    updateEDGEpgminfo(main_prog + "-" + app_val, oProcstarttime, oProcendtime, "ENSIS4020AJ", mcon2);
                                }
                                updateEDGEpgminfo(tot_pgval, oProcstarttime, oProcendtime, "ENSIS4020AJ", mcon2);
                            }
                        }
                        else if (!ProgName.Contains("-") && ProgName.Contains("+"))
                                      {
                                          String[] prog_split = ProgName.Split('+');
                                          string main_prog = prog_split[0];
                            updateEDGEpgminfo_match(main_prog, oProcstarttime, oProcendtime, "ENSIS4020AJ", mcon2);

                                          int counter_val = 1;

                                          foreach (string app_val in prog_split)
                                          {
                                              if (counter_val != 1)
                                              {
                                                  int len_newval = app_val.Length;
                                                  string new_pg = main_prog.Substring(0, (main_prog.Length - len_newval)) + app_val;
                                            updateEDGEpgminfo_match(new_pg, oProcstarttime, oProcendtime, "ENSIS4020AJ", mcon2);

                                              }
                                              counter_val++;
                                          }

                                      }
                                    else if (ProgName.Contains("-") && ProgName.EndsWith("P"))
                                    {
                                        String[] prog_split = ProgName.Split('-');
                                        string main_prog = prog_split[0];
                                        int count = 1;
                                        foreach (string app_val in prog_split)
                                        {
                                            if (count != 1)
                                            {
                                                string app_val1 = app_val.Replace("P","");
                                                updateEDGEpgminfo(main_prog + "-" + app_val1, oProcstarttime, oProcendtime, "ENSIS4020AJ", mcon2);

                                            }
                                            count++;
                                        }

                                    }
                                    else if (!ProgName.Contains("-") && !ProgName.Contains("+"))
                                    {
                                        updateEDGEpgminfo_match(ProgName, oProcstarttime, oProcendtime, "ENSIS4020AJ", mcon2);
                                  
                                    }
                                    else
                                        updateEDGEpgminfo(ProgName, oProcstarttime, oProcendtime, "ENSIS4020AJ", mcon2);
}*/

                    string CmdText = "INSERT IGNORE INTO csvextract VALUES(@idcsv,@date,@machname,@progname,@procstrtime,@procendtime,@proctime,@machoprtime,@laserproctime,@nctproctime,@alarmtime,@matname,@thickness,@sheetx,@sheety,@yieldrate,@good,@ngqty,@speed,@proctimesecs,@alarmtimesecs,@oprtimesecs)";
                    MySqlCommand cmd = new MySqlCommand(CmdText, mcon);

                    cmd.Parameters.AddWithValue("@idcsv", oDate + "_" + "ens" +counter);
                    cmd.Parameters.AddWithValue("@date", Date);
                    cmd.Parameters.AddWithValue("@machname", Encoding.UTF8.GetBytes(MachName));
                    cmd.Parameters.AddWithValue("@progname", ProgName);
                    cmd.Parameters.AddWithValue("@procstrtime", Procstarttime);
                    cmd.Parameters.AddWithValue("@procendtime", Procendtime);
                    cmd.Parameters.AddWithValue("@proctime", ProcTime);
                    cmd.Parameters.AddWithValue("@machoprtime", MachOprTime);
                    cmd.Parameters.AddWithValue("@laserproctime", LaserProcTime);
                    cmd.Parameters.AddWithValue("@nctproctime", NCTProcTime);
                    cmd.Parameters.AddWithValue("@alarmtime", AlarmTime);
                    cmd.Parameters.AddWithValue("@matname", MatName);
                    cmd.Parameters.AddWithValue("@thickness", Thickness);
                    cmd.Parameters.AddWithValue("@sheetx", SheetX);
                    cmd.Parameters.AddWithValue("@sheety", SheetY);
                    cmd.Parameters.AddWithValue("@yieldrate", YieldRate);
                    cmd.Parameters.AddWithValue("@good", oGood);
                    cmd.Parameters.AddWithValue("@ngqty", oNGQty);
                    cmd.Parameters.AddWithValue("@speed", Speed);
                    cmd.Parameters.AddWithValue("@proctimesecs", ProcTimeSecs);
                    cmd.Parameters.AddWithValue("@alarmtimesecs", AlarmTimeSecs);

                    cmd.Parameters.AddWithValue("@oprtimesecs", OprTimeSecs);

                    cmd.ExecuteNonQuery();
                    counter++;
                }

            }
        }
    }
}
    

